package me.shpp.ddubovitsky.kotolin.ui.holders

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.GridView
import me.shpp.ddubovitsky.kotolin.ui.views.StaticGridView

/**
 * Created by Dubovitsky Denis on 5/28/2017.
 */

class CardHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val cvCard: CardView
    val gridView: StaticGridView

    init {
        cvCard = itemView as CardView
        gridView = cvCard.getChildAt(0) as StaticGridView
    }

    companion object {
        private val TAG = CardHolder::class.java.simpleName
    }

}

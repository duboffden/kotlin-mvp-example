package me.shpp.ddubovitsky.kotolin.data.network

import io.reactivex.Observable
import retrofit2.Retrofit



/**
 * Created by Dubovitsky Denis on 5/31/2017.
 */
class AppApiHelper(private val mRetrofit: Retrofit) : ApiHelper {

    override fun getImages(): Observable<ArrayList<ArrayList<String>>>{
        return mRetrofit.create(ApiHelper::class.java).getImages();
    }
}
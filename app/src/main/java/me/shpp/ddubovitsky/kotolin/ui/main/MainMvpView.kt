package me.shpp.ddubovitsky.kotolin.ui.main

/**
 * Created by Dubovitsky Denis on 5/31/2017.
 */

interface MainMvpView{
    fun updateImages(s: ArrayList<ArrayList<String>>?)

    fun showErrorSnackbar()

    fun hideLoadMoreButton()

    fun showLoadMoreButton()

    fun showRecyclerView()

    fun hideRecyclerView()

    fun hideRefreshing()

    fun isRefreshing(): Boolean

    fun startRefreshing()
}
package me.shpp.ddubovitsky.kotolin.di.modules

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import me.shpp.ddubovitsky.kotolin.constants.Constants
import me.shpp.ddubovitsky.kotolin.data.network.ApiHelper
import me.shpp.ddubovitsky.kotolin.data.network.AppApiHelper
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Dubovitsky Denis on 5/29/2017.
 */

@Module
class NetModule {

    @Provides
    internal fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_API_URL)
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    fun provideApiHelper(r: Retrofit): ApiHelper {
        return AppApiHelper(r)
    }
}

package me.shpp.ddubovitsky.kotolin.constants

/**
 * Created by Dubovitsky Denis on 5/29/2017.
 */

object Constants {
    //apis
    val BASE_API_URL: String = "http://new.shpp.me:9347"
    //views
    val GRID_NUM_COLUMNS = 3;
    //animations
    val ANIMATION_DURATION: Long = 200;
    val ALPHA_HALF_VISIBLE = 0.5f;
    val ALPHA_VISIBLE = 1f;
    val ALPHA_HIDDEN = 0f;
    //bundles
    val BUNDLE_LOADED_IMAGES_ARRAY ="BUNDLE_LOADED_IMAGES_ARRAY"
    val BUNDLE_IS_REFRESHING = "BUNDLE_IS_REFRESHING"
    val BUNDLE_IS_SHOWING_RECYCLER_VIEW = "BUNDLE_IS_SHOWING_RECYCLER_VIEW"
    val BUNDLE_LIST_STATE = "BUNDLE_LIST_STATE"
}

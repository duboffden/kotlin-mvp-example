package me.shpp.ddubovitsky.kotolin.ui.adapters

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.shpp.ddubovitsky.kotolin.R
import me.shpp.ddubovitsky.kotolin.constants.Constants
import me.shpp.ddubovitsky.kotolin.constants.Constants.ANIMATION_DURATION
import me.shpp.ddubovitsky.kotolin.constants.Constants.ALPHA_HALF_VISIBLE
import me.shpp.ddubovitsky.kotolin.constants.Constants.ALPHA_VISIBLE
import me.shpp.ddubovitsky.kotolin.ui.holders.CardHolder
import me.shpp.ddubovitsky.kotolin.ui.views.StaticGridView
import java.util.*


/**
 * Created by Dubovitsky Denis on 5/28/2017.
 *
 */

class RecyclerCardsAdapter(private val mContext: Context) : RecyclerView.Adapter<CardHolder>() {

    internal var mImagesLinks = ArrayList<ArrayList<String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardHolder {
        return CardHolder(makeItem())
    }

    override fun onBindViewHolder(holder: CardHolder, position: Int) {
        (holder.gridView.adapter as ImagesGridAdapter).setImages(mImagesLinks[position])
        setAnimation(holder.cvCard)
    }

    fun updateImages(im: ArrayList<ArrayList<String>>) {
        mImagesLinks = im
        notifyDataSetChanged()
    }

    private fun makeItem(): CardView {
        val cardView = CardView(mContext)
        addStylesToCardView(cardView)

        val staticGridView = StaticGridView(mContext)
        addStylesToGrid(staticGridView)

        staticGridView.setAdapter(ImagesGridAdapter(mContext, 0, ArrayList<String>()))

        cardView.addView(staticGridView)
        return cardView
    }

    private fun addStylesToCardView(cardView: CardView) {
        cardView.useCompatPadding = true
        cardView.setCardBackgroundColor(Color.WHITE)
        cardView.layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun addStylesToGrid(staticGridView: StaticGridView) {
        val spacing = mContext.resources.getDimension(R.dimen.grid_spacing).toInt()
        staticGridView.setNumColumns(Constants.GRID_NUM_COLUMNS)
        staticGridView.setHorizontalSpacing(spacing)
        staticGridView.setVerticalSpacing(spacing)
    }

    private fun setAnimation(viewToAnimate: View) {
        viewToAnimate.clearAnimation()
        viewToAnimate.alpha = ALPHA_HALF_VISIBLE;
        viewToAnimate.animate().alpha(ALPHA_VISIBLE).duration = ANIMATION_DURATION;
    }

    override fun getItemCount(): Int {
        return mImagesLinks.size
    }
}

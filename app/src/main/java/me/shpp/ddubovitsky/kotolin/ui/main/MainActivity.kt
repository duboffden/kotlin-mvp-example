package me.shpp.ddubovitsky.kotolin.ui.main

import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import me.shpp.ddubovitsky.kotolin.MvpApp
import me.shpp.ddubovitsky.kotolin.R.id.btn_load
import me.shpp.ddubovitsky.kotolin.R.id.rl_root_main_activity
import me.shpp.ddubovitsky.kotolin.R.layout.activity_main
import me.shpp.ddubovitsky.kotolin.constants.Constants
import me.shpp.ddubovitsky.kotolin.constants.Constants.ANIMATION_DURATION
import me.shpp.ddubovitsky.kotolin.constants.Constants.ALPHA_HIDDEN
import me.shpp.ddubovitsky.kotolin.constants.Constants.ALPHA_VISIBLE
import me.shpp.ddubovitsky.kotolin.constants.Constants.BUNDLE_LIST_STATE
import me.shpp.ddubovitsky.kotolin.ui.adapters.RecyclerCardsAdapter
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainMvpView {

    @BindView(rl_root_main_activity)
    lateinit var rlRoot: RelativeLayout;
    @BindView(btn_load)
    lateinit var btnLoadMore: Button;

    @Inject
    lateinit var mMainMvpPresenter: MainMvpPresenter;

    lateinit var mCardsAdapter: RecyclerCardsAdapter
    lateinit var srRefresh: SwipeRefreshLayout;
    lateinit var rvImages: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_main)

        ButterKnife.bind(this)
        (applicationContext as MvpApp).appComponent.inject(this)

        setupRecyclerView();

        mMainMvpPresenter.attach(this)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(Constants.BUNDLE_LIST_STATE, getListState())
        mMainMvpPresenter.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        restoreListState(savedInstanceState.getParcelable(BUNDLE_LIST_STATE))
        mMainMvpPresenter.restoreState(savedInstanceState)
    }

    @OnClick(btn_load)
    fun loadImages(v: android.view.View) {
        mMainMvpPresenter.loadImagesForButtons()
    }

    fun setupRecyclerView() {
        rvImages = RecyclerView(this)
        addStylesToRecyclerView(rvImages)
        rvImages.layoutManager = LinearLayoutManager(this)

        mCardsAdapter = RecyclerCardsAdapter(applicationContext)
        rvImages.adapter = mCardsAdapter;

        srRefresh = SwipeRefreshLayout(this)

        srRefresh.setOnRefreshListener { mMainMvpPresenter.loadImagesForButtons() }

        srRefresh.addView(rvImages)
        rlRoot.addView(srRefresh)
    }

    private fun addStylesToRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    override fun updateImages(s: ArrayList<ArrayList<String>>?) {
        if (s == null)
            return;

        mCardsAdapter.updateImages(s)
    }

    override fun hideRefreshing() {
        if (srRefresh.isRefreshing)
            srRefresh.isRefreshing = false;
    }

    override fun showErrorSnackbar() {
        Snackbar.make(rlRoot, "Error while loading :c", Snackbar.LENGTH_SHORT).show();
    }

    override fun hideLoadMoreButton() {
        hideView(btnLoadMore)
    }

    override fun showLoadMoreButton() {
        showView(btnLoadMore)
    }

    override fun hideRecyclerView() {
        hideView(rvImages)
    }

    override fun showRecyclerView() {
        showView(rvImages);
    }

    override fun isRefreshing(): Boolean = srRefresh.isRefreshing

    override fun startRefreshing() {
        srRefresh.isRefreshing = true;
    }

    fun hideView(v: View) {
        v.animate().alpha(ALPHA_HIDDEN).duration = ANIMATION_DURATION
        v.isEnabled = false;
    }

    fun showView(v: View) {
        v.animate().alpha(ALPHA_VISIBLE).duration = ANIMATION_DURATION
        v.isEnabled = true;
    }

    fun getListState(): Parcelable = rvImages.layoutManager.onSaveInstanceState()

    fun restoreListState(state: Parcelable?) {
        if (state == null)
            return;

        rvImages.layoutManager.onRestoreInstanceState(state)
    }
}

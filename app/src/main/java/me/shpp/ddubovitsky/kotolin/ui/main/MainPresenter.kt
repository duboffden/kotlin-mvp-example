package me.shpp.ddubovitsky.kotolin.ui.main

import android.os.Bundle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.shpp.ddubovitsky.kotolin.constants.Constants
import me.shpp.ddubovitsky.kotolin.data.network.ApiHelper


/**
 * Created by Dubovitsky Denis on 5/31/2017.
 */
@Suppress("UNCHECKED_CAST")
class MainPresenter(var mApiHelper: ApiHelper) : MainMvpPresenter {

    private var mMainMvpView: MainMvpView? = null

    private var mImagesLinks: ArrayList<ArrayList<String>>? = null

    private var isShowingRecyclerView = false;

    override fun attach(mainMvpView: MainMvpView) {
        this.mMainMvpView = mainMvpView
    }

    override fun loadImagesForButtons() {
        mApiHelper.getImages()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ images ->
                    if (images == null)
                        return@subscribe

                    onLoad(images)
                }, { error ->
                    error.printStackTrace()

                    onError()

                });
    }

    private fun onLoad(images: ArrayList<ArrayList<String>>) {
        mImagesLinks = images;
        mMainMvpView?.hideLoadMoreButton()
        mMainMvpView?.hideRefreshing()
        mMainMvpView?.showRecyclerView()
        updateViewWithImages();
        isShowingRecyclerView = true;
    }

    private fun onError() {
        mMainMvpView?.showErrorSnackbar();
        mMainMvpView?.showLoadMoreButton();
        mMainMvpView?.hideRefreshing()
        mMainMvpView?.hideRecyclerView()
        isShowingRecyclerView = false;
    }

    private fun updateViewWithImages() {
        mMainMvpView?.updateImages(mImagesLinks)
    }

    override fun saveState(savingState: Bundle) {
        savingState.putSerializable(Constants.BUNDLE_LOADED_IMAGES_ARRAY, mImagesLinks)

        var refreshing: Boolean? = mMainMvpView?.isRefreshing()

        if (refreshing == null)
            refreshing = false;

        savingState.putBoolean(Constants.BUNDLE_IS_REFRESHING, refreshing)

        savingState.putBoolean(Constants.BUNDLE_IS_SHOWING_RECYCLER_VIEW, isShowingRecyclerView)
    }

    override fun restoreState(savedState: Bundle) {
        mImagesLinks = savedState.getSerializable(Constants.BUNDLE_LOADED_IMAGES_ARRAY) as ArrayList<ArrayList<String>>?
        updateViewWithImages()

        if (savedState.getBoolean(Constants.BUNDLE_IS_SHOWING_RECYCLER_VIEW)) {
            mMainMvpView?.hideLoadMoreButton()
            mMainMvpView?.showRecyclerView()
            isShowingRecyclerView = true;
        } else {
            mMainMvpView?.hideRecyclerView()
            mMainMvpView?.showLoadMoreButton()
            isShowingRecyclerView = false;
        }

        if (savedState.getBoolean(Constants.BUNDLE_IS_REFRESHING))
            mMainMvpView?.startRefreshing()
    }
}
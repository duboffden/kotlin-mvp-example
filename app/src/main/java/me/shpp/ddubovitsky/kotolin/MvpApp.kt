package me.shpp.ddubovitsky.kotolin

import android.app.Application
import me.shpp.ddubovitsky.kotolin.di.component.AppComponent
import me.shpp.ddubovitsky.kotolin.di.component.DaggerAppComponent
import me.shpp.ddubovitsky.kotolin.di.modules.NetModule
import me.shpp.ddubovitsky.kotolin.di.modules.PresentersModule

/**
 * Created by Dubovitsky Denis on 5/29/2017.
 */

class MvpApp : Application() {
    lateinit var appComponent: AppComponent;

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .netModule(NetModule())
                .presentersModule(PresentersModule())
                .build()
    }
}

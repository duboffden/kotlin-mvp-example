package me.shpp.ddubovitsky.kotolin.ui.main


/**
 * Created by Dubovitsky Denis on 5/31/2017.
 */
interface MainMvpPresenter {
    fun loadImagesForButtons()
    fun attach(mainMvpView: MainMvpView)
    fun saveState(savingState: android.os.Bundle)
    fun restoreState(savedState: android.os.Bundle)
}
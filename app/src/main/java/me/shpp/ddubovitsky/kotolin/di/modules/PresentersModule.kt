package me.shpp.ddubovitsky.kotolin.di.modules

import dagger.Module
import dagger.Provides
import me.shpp.ddubovitsky.kotolin.data.network.ApiHelper
import me.shpp.ddubovitsky.kotolin.ui.main.MainMvpPresenter
import me.shpp.ddubovitsky.kotolin.ui.main.MainPresenter

/**
 * Created by Dubovitsky Denis on 5/29/2017.
 */

@Module
class PresentersModule {

    @Provides
    internal fun providesMainPresenter(mApiHelper: ApiHelper): MainMvpPresenter {
        return MainPresenter(mApiHelper)
    }
}

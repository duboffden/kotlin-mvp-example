package me.shpp.ddubovitsky.kotolin.ui.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.ArrayAdapter
import android.widget.ImageButton

import com.squareup.picasso.Picasso
import me.shpp.ddubovitsky.kotolin.R

import java.util.ArrayList

/**
 * Created by Dubovitsky Denis on 5/28/2017.
 */

class ImagesGridAdapter(context: Context, i: Int, internal var images: ArrayList<String>)
    : ArrayAdapter<Int>(context, i) {

    fun setImages(m: ArrayList<String>) {
        images = m
        notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // TODO Auto-generated method stub
        var label: ImageButton? = (convertView as ImageButton?)


        if (label == null)
            label = ImageButton(context)


        Picasso.with(context).load(images[position])
                .fit()
                .placeholder(R.drawable.image_first)
                .error(R.drawable.image_first)
                .into(label)

        addStylesToIb(label)
        return label
    }


    private fun addStylesToIb(imageButton: ImageButton) {
        val sideLength: Int = context.resources.getDimension(R.dimen.grid_image_height_width).toInt();
        imageButton.layoutParams = AbsListView.LayoutParams(sideLength, sideLength)
    }

    override fun getCount(): Int {
        return images.size
    }
}

package me.shpp.ddubovitsky.kotolin.di.component

import dagger.Component
import me.shpp.ddubovitsky.kotolin.di.modules.NetModule
import me.shpp.ddubovitsky.kotolin.di.modules.PresentersModule
import me.shpp.ddubovitsky.kotolin.ui.main.MainActivity
import javax.inject.Singleton

/**
 * Created by Dubovitsky Denis on 5/29/2017.
 */
@Singleton
@Component(modules = arrayOf(NetModule::class, PresentersModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}

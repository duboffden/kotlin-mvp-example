package me.shpp.ddubovitsky.kotolin.data.network

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Dubovitsky Denis on 5/31/2017.
 */
interface ApiHelper{
    @GET("images")
    fun getImages(): Observable<ArrayList<ArrayList<String>>>;
}
